export const universo_projekto = `
query ($universoprojektoposedanto_PosedantoKomunumo_Id: Float!, $first: Int) {
  universoProjekto(first: $first, universoprojektoposedanto_PosedantoKomunumo_Id: $universoprojektoposedanto_PosedantoKomunumo_Id) {
    edges {
      node {
        uuid
        nomo {
          enhavo
        }
        priskribo {
          enhavo
        }
        tipo {
          nomo {
            enhavo
          }
        }
        pozicio
        realeco {
          nomo {
            enhavo
          }
        }
        kategorio {
          edges {
            node {
              nomo {
                enhavo
              }
            }
          }
        }
        sxablono {
          id
        }
        statuso {
          nomo {
            enhavo
          }
        }
        objekto {
          nomo {
            enhavo
          }
        }
        universoprojektoposedantoSet {
          edges {
            node {
              projekto {
                nomo {
                  enhavo
                }
              }
            }
          }
        }
        tasko {
          edges {
            node {
              id
              nomo {
                enhavo
              }
              priskribo {
                enhavo
              }
            }
          }
        }
      }
    }
  }
}

`
  export const universo_projekto_byUUID =`
  query ($uuid: UUID) {
  universoProjekto(uuid: $uuid) {
    edges {
      node {
        uuid
        nomo {
          enhavo
        }
        priskribo {
          enhavo
        }
        tipo {
          nomo {
            enhavo
          }
        }
        pozicio
        realeco {
          nomo {
            enhavo
          }
        }
        kategorio {
          edges {
            node {
              nomo {
                enhavo
              }
            }
          }
        }
        sxablono {
          id
        }
        statuso {
          nomo {
            enhavo
          }
        }
        objekto {
          nomo {
            enhavo
          }
        }
        universoprojektoposedantoSet {
          edges {
            node {
              projekto {
                nomo {
                  enhavo
                }
              }
            }
          }
        }
        tasko {
          edges {
            node {
              id
              nomo {
                enhavo
              }
              priskribo {
                enhavo
              }
            }
          }
        }
      }
    }
  }
}


  `
